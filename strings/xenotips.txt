Hive Beacons can be used to flank the FOB while marines are busy on the frontlines.
Acid pillars can be sneakily placed next to a door in order to surprise marines.
Alien structures like pylons, walls, or pillars are absolutely vital to your victory, be it as cover or to delay and funnel marines.
Always thank your drones and hivelords for supporting the hive!
Don't underestimate survivors. They have no armor but that makes them very fast, they're inherently hardier than marines and have various tricks up their sleeves.
While the Queen is de-ovied, the hive's techpoints gain rate is reduced.
Try out new castes or strains that you might have passed up initially. You might find them to be surprisingly fun.
As an Alien, it is EXTREMELY IMPORTANT that you have your abilities set up on hotkeys or macros. Ask for help if you don't know how.
Pouncing or lunging into a marine that's almost offscreen is not very wise. A marine with a shotgun may be right next to them.
Most alien deaths are caused by over-aggression. Rein yourself in, or you may find yourself dying round after round.
You can spit at or shoot a welding fuel tank to blow it up next to hostiles.
Quickly rest if you're right next to an explosive about to blow, you'll absorb half the explosion damage. The same method can be used to avoid shrapnel from certain explosion types.