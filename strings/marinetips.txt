You can link plasteel barricades together using a crowbar. First click one, and then click one adjacent. Repeat as needed.
A lone marine is a dead marine and a happy xeno. Stick together!
Watch out for friendly fire. Make sure you're not walking into anyone's firing line, and make sure no one else is in yours!
Be mindful when using hand grenades. Getting stunned or knocked over after priming a grenade will drop it at your feet. This is obviously very dangerous for not only you, but your friends as well.
Reflective walls will bounce back non-IFF bullets, including rockets. Try not to shoot them.
Vary your loadout! Doing the same thing every round gets old fast. Try out the pistols, or carry flares.
Be careful when firing an Orbital Bombardment, Close Air Support, or the Railgun. Make sure you're not about to blow up the entire marine push.
Always assume the Orbital Bombardment is closer than it looks.
Never inject a marine you haven't scanned! You might overdose them; ESPECIALLY so with an emergency autoinjector.
Check perma-dead marines (skull icon on medhud) for useful gear, such as webbings, AP ammo, or binoculars.
Always check for traps beneath loose objects.
Bring a fire extinguisher, it might just save someone's life. They can remove flames that block a push, remove acid from other marines, as well as put out people on fire.
Trading your life for a Xenomorph's is almost always worth it.
If another marine is stunned, you can click on them with help intent with an open hand to shake them up faster.
No round has ever been won behind a barricade.
Always clear weeds and alien structures as you advance.
Try not to block off pushes when using a flamethrower.
Recover bodies whenever possible. Even if the marine is unrevivable, useful gear and ammunition can still be salvaged.
A lack of supplies at the FOB can cost you the round. Make sure to unpack supply crates, weapons, and ammo for your fellow marines.
More dakka is not good when it is directed towards your teammates.
Never place barricades facing eachother. That blocks bullets, and both get damaged at once if slashed.
An FOB is as strong as its weakest part. Secure all flanks, remember that walls can be pried open.
Flamethrowers: Green fire is weak and should be used for weed clearing. Queens and Ravagers are flameproof. Most flamethrower damage comes from the initial burst, not the flame on the ground. Don't forget you have an underbarrel extinguisher.
CPR extends the defib timer on dead marines. It can be done by clicking the dead marine on help intent with an open hand. Make sure to get the rhythm correct!
If the main push is stagnating and the area is congested, try to coordinate with a few others and go to a different area.
When pushing into an area, check the flanks. Especially so in caves.